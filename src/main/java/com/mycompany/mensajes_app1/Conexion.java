/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author monic
 */
public class Conexion {
    // get_conection va a devolver un objeto
    public Connection get_connection(){
        Connection conection = null;
        try{
            conection = DriverManager.getConnection("idbc:mysql://localhost:3306/mensajes_app","root","");
            if(conection!=null){
                System.out.println("Conexión exitosa");
            }
        }catch (SQLException e){// esto es para caputar la pantalla y mostrarla en pantalla con el print....
                System.out.println(e);
        }
        return conection;
    }
}
